
local MakePlayerCharacter = require "prefabs/player_common"

local assets = {Asset("SCRIPT", "scripts/prefabs/player_common.lua")}
local prefabs = {}

-- Custom starting inventory
local start_inv = {"flint", "flint", "twigs", "twigs"}

-- When the character is revived from human
local function onbecamehuman(inst)
    -- Set speed when not a ghost (optional)
    inst.components.locomotor:SetExternalSpeedMultiplier(inst, "lai_speed_mod",
                                                         1)
end

local function onbecameghost(inst)
    -- Remove speed modifier when becoming a ghost
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst,
                                                            "lai_speed_mod")
end

-- When loading or spawning the character
local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end


-- This initializes for both the server and client. Tags can be added here.
local common_postinit = function(inst)
    -- Minimap icon
    inst.MiniMapEntity:SetIcon("lai.tex")
end

-- local function speedadjustment(inst)
--     if (inst.components.health:GetPercent() > .9) then
--         inst.components.locomotor.walkspeed = 4.8
--         inst.components.locomotor.runspeed = 7.2
--     elseif (inst.components.health:GetPercent() > .7) then
--         inst.components.locomotor.walkspeed = 4.6
--         inst.components.locomotor.runspeed = 6.9
--     elseif (inst.components.health:GetPercent() > .5) then
--         inst.components.locomotor.walkspeed = 4.4
--         inst.components.locomotor.runspeed = 6.4
--     end
-- end

local function Regen(inst)
    if not inst.components.hunger:IsStarving(inst) then
        inst.components.health:StartRegen(0.125, 0.5)
    elseif inst.components.hunger:IsStarving(inst) then
        inst.components.health:StopRegen()
    end
end


-- This initializes for the server only. Components are added here.
local master_postinit = function(inst)
    -- choose which sounds this character will play
    inst.soundsname = "willow"

    -- Uncomment if "wathgrithr"(Wigfrid) or "webber" voice is used
    -- inst.talker_path_override = "dontstarve_DLC001/characters/"

    -- Stats	
    inst.components.health:SetMaxHealth(80)
    inst.components.hunger:SetMax(100)
    inst.components.sanity:SetMax(150)

    -- Reduces likelihood of getting frozen solid (higher values reduce risk)
    inst.components.freezable:SetResistance(250)

    -- Higher values reduce loss of body heat

    -- Damage multiplier (optional)
    inst.components.combat.damagemultiplier = 1.25

    -- Hunger rate (optional)
    inst.components.hunger.hungerrate = 1.15 * TUNING.WILSON_HUNGER_RATE

    -- Setspeed
    inst.components.locomotor.walkspeed = 6
    inst.components.locomotor.runspeed = 7

    inst:DoPeriodicTask(1, Regen, nil, inst)

    inst.OnLoad = onload
    inst.OnNewSpawn = onload

end

return MakePlayerCharacter("lai", prefabs, assets, common_postinit,
                           master_postinit, start_inv)

